archived/network
================

**NOTE**:
Something is wrong with this role.
It will probably freeze up your DNS queries and no hostnames will be resolved in anything that routes DNS requests through `systemd`.
Do not reuse as is.
