#!/bin/sh
#
# Barebones Asciidoctor docs deployment. Literally, barebones.


# Always purge old site source and then start from docs source directory.
rm -rf public/
cd "$(dirname "$0")/docs/"


# Generate HTML pages for each AsciiDoc source file.
for doc in *; do
    asciidoctor \
        --backend=html5 \
        --doctype=article \
        --out-file _build/$(echo $doc | sed --expression='s/.adoc//')/index.html \
        $doc
done


# Move generated HTML pages to expected path by GitLab Pages CI.
mv _build/ ../public/
